//unul dintre cele mai misto exercitii pe care le-am facut

package Ro.Orange;

import java.io.IOException;

public class Test {
    public static void main(String[] args) {

        //obiect de tip Create file
        CreateFile fisier = new CreateFile();

        //creez fisierul
        fisier.openFile();

        //findca myRecords poate da eroare, pun codul in try-catch
        try {
            fisier.myRecords();
        } catch (IOException e) {
            System.out.println("Oops, error!");
        }

        //inchid fisierul presupunand ca prin close file se refera la a inchide stream-ul
        fisier.closeFile();

    }
}
