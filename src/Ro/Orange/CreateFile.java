package Ro.Orange;

//import clasele de care am nevoie
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.Formatter;
import java.util.Scanner;

public class CreateFile {
        //citesc inputul user-ului pentru orice
        private Scanner input;

        //un array care tine elementele ce trebuie scrise in fisier
        private String[] myArray;

        //o variabila care tine calea absoluta unde trebuie creat fisierul
        private String path;

        //obiect nou pentru a putea crea fisierul
        private File newFile;

        //obiect nou pentru a scrie in fisier
        private BufferedWriter writeToFile;

        //n-am inteles care-i scopul astuia
        private Formatter x = new Formatter();

        //metoda care creaza fisierul
        public void openFile() {

            //initializez citirea inputului de la user
            input = new Scanner(System.in);
            System.out.println("Please type the absolute path of the place where you want the file to be created");

            //stochez calea
            path = input.next();
            //C:\JavaFiles\fileWasActuallyCreatedFromJava.txt
            //asta-i calea mea. cand testati puneti calea intr-un dosar care exista deja altfel o sa crape

            //in try-catch creez fisierul
            try {

            //initializez obiectul de tip file cu calea stocata in path
            newFile = new File(path);

            //creez fisierul
            newFile.createNewFile();
                System.out.println("The file was created.");

            //prindem erori
            } catch(FileAlreadyExistsException e) {
                System.out.println("File already exists.");
            } catch (IOException e) {
                System.out.println("Another I/O error");}
        }

        //functia asta initializeaza array-ul care va tine elementele ce trebuie scrise in fisier si le scrie in fisier
        //pe linie noua
        // mi-a fost lene sa pun try-catch in ea, asa ca arunca orice eroare i/o sa se descurce altcineva cu ea :))
        public void myRecords() throws IOException{

            //vreau un obiect nou de tip scanare input aici
            input = new Scanner(System.in);
            System.out.println("Enter the number of elements you want to write to the file.");

            //stochez numarul intreg scris de user intr-o variabila
            int arrayLength = input.nextInt();

            //initializez array-ul si ii dau lungimea setata mai sus de user
            myArray= new String[arrayLength];

            //pentru fiecare pozitie a array-ului rog useru-ul sa introduca un nou elemnt
            for (int i = 0; i < myArray.length ; i++) {
                System.out.println("Enter a new element.");
                String element = input.next();
                myArray[i] = element;

                //ii arat ce a introdus
                System.out.println("Entered element is [" + myArray[i].toString() + "]");
            }

            //ii arat toate elementele introduse care vor fi scrise in fisier
            System.out.println("The elements that will be written to the file are: " + Arrays.toString(myArray));

            //initializez obiectul care va scrie in fisier
            writeToFile = new BufferedWriter(new FileWriter(path));

            //pentru fiecare element al array-ului scriu cate o linie in fisier
            for (String element:myArray) {
                writeToFile.write(element);
                writeToFile.newLine();
            }



        }

        //metoda asta inchide fisierul
        public void closeFile(){
            try {

        //inchid stream-ul deschis in myRecords presupunand ca la asta se refera prin closeFile
                writeToFile.close();

        //verifica locatia unde a fost creat fisierul
        // chiar este acolo si contine ce i-ai zis tu mai sus. mi se pare foarte tare :)
                System.out.println("Check the file in " + path);
            } catch (IOException e) {
                System.out.println("Oops, error!");
            }


    }


}
